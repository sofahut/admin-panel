import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";


const routes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "Login",
    component: () => import('./views/Login.vue'),
    meta: { layout: "empty" },
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: () => import('./views/Dashboard.vue'),
  },
  {
    path: "/users",
    name: "Users",
    component: () => import('./views/Users.vue'),
  },
  {
    path: "/products",
    name: "Products",
    component: () => import('./views/Products.vue'),
  },
  {
    path: "/orders",
    name: "Orders",
    component: () => import('./views/Orders.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

export default router;
